// Import questions model
let Questions = require('../reflection-model/questionModel');


// Handle get All Questions /Get function
exports.findAll = function(req, res) {
    Questions.get((err, questions) => {
        if(err)
        {
            res.json({
                "status" : "error",
                "message" : err,
            })
        }
        res.json({
            "status" : "success",
            "message" : "All Questions successfuly retrieved",
            "data" : questions
        })
    })
}