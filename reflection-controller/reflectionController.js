// Import userreflection model
let UserReflection = require('../reflection-model/userReflectionModel');

// Handle index actions / Get function
exports.getAll= function (req, res) {
    UserReflection.get((err, userreflections) => {
        if(err)
        {
            res.json({
                "status" : "error",
                "message" : err,
            })
        }
        res.json({
            "status" : "success",
            "message" : "User Reflections retrieved successfuly",
            "data" : userreflections
        })
    })
}

// Handle create new user reflection
exports.createNew = function(req, res) {
    var userreflection = new UserReflection();
    userreflection.userid = req.body.userid;
    userreflection.username = req.body.username;
    userreflection.answer1 = req.body.answer1;
    userreflection.answer2 = req.body.answer2;
    userreflection.answer3 = req.body.answer3;

    userreflection.save( err => {
            if(err)
            {
                res.json(err);
            }

            res.json({
                "message" : "New user reflection added",
                data : userreflection
            })
    })
    
}

// Handle find single reflection by id
exports.getSingle = function(req, res) {
    UserReflection.findById(req.params.ide, (err, userreflection) => {
        if(err)
        {
            res.send(err);
        }

        res.json({
            "message" : "User Reflection Loading",
            data : userreflection
        })
    } )
}


// Handle update single reflection by id

exports.updateSingle = function(req, res) {
    UserReflection.findById(req.params.ide, (err, userreflection) => {
        if(err)
        {
            res.send(err);
        }

        userreflection.username = req.body.username;
        userreflection.answer1 = req.body.answer1;
        userreflection.answer2 = req.body.answer2;
        userreflection.answer3 = req.body.answer3;

        //  userreflection.update_date = Date.now
        

        userreflection.save( err => {
            if(err){
                res.send(err);
            }

            res.json({
                "message": "User Reflection updated",
                "data" : userreflection
            })
        })
    })
}


// Handle delete single reflection by id 

exports.deleteSingle = function (req, res) {
    UserReflection.remove({
        _id: req.params.ide
    }, function (err, userreflection) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'Relection deleted'
        });
    });
};




























