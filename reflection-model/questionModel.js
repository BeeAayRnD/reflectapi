// Import mongoose
var mongoose = require('mongoose');

// Create schema
var questionSchema = mongoose.Schema({
    
    // "create_date":{"$date":"2021-06-23T12:52:59.369Z"},"update_date":{"$date":"2021-06-23T12:52:59.369Z"}}

    _id : {
        type : String,
    },

    category : {
        type : String,
    },

    type : {
        type : String,
    },
    
    difficulty : {
        type: String,
    },

    question : {
        type : String,
    },

    description : {
        type : String,
    },
    explanation : {
        type: String,
    },

    tip : {
        type : String,
    },

    tags : {
        type : Array,
    },

    correct_answer : {
        type : String,
    },

    incorrect_answers : {
        type : Array,
    },

    create_date : {
        type : Date,
    },

    update_date : {
        type: Date,
    }

});

// Export userReflectionSchema 
var Questions = module.exports = mongoose.model('questions', questionSchema);

module.exports.get = function (callback, limit) {
    Questions.find(callback).limit(limit);
}
