// Import mongoose
var mongoose = require('mongoose');

// Create schema
var userReflectionSchema = mongoose.Schema({
    
    userid : {
        type : String,
        required : true
    },

    username : {
        type : String,
        required : true
    },

    answer1 : {
        type : String,
        required: true
    },

    answer2 : {
        type : String,
    },

    answer3 : {
        type : String,
    },

    create_date: {
        type: Date,
        default: Date.now
    },
    
    update_date: {
        type: Date,
        default: Date.now
    }

});

// Export userReflectionSchema 
var UserReflection = module.exports = mongoose.model('userreflection', userReflectionSchema);

module.exports.get = function (callback, limit) {
    UserReflection.find(callback).limit(limit);
}
