// Step 1 - Initialise express router 
let router = require('express').Router();

// Step 2 - Send default API response on root endpoint /
router.get('/', (req, res) => {
    res.json({
        "status" : "Reflection API is working",
        "message" : "Hello from Reflection API, built using Node, Express and Mongo"
    });
});

// Import userreflection controller
let userReflectionController = require('../reflection-controller/reflectionController');

// Import question controller
let questionController = require('../reflection-controller/questionController');

// User Reflection routes
router.route('/userreflections')
      .get(userReflectionController.getAll)
      .post(userReflectionController.createNew);


router.route('/userreflections/:ide')
      .get(userReflectionController.getSingle)
      .put(userReflectionController.updateSingle)
      .delete(userReflectionController.deleteSingle);


router.route('/questions')
      .get(questionController.findAll);

// Step 3 - Export API routes
module.exports = router;

