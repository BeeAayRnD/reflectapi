// Step 1 - Import Express
let express = require('express');

// Import body-parser*
let bodyParser = require('body-parser');

// Import mongoose*
let mongoose = require('mongoose');

// Step 2 - Initialise the app 
let app = express();

// Import routes *
let routes = require('./reflection-routes/routes');

// Configure bodyParser to handle post requests*
app.use(bodyParser.urlencoded({
    extended : true
}));

app.use(bodyParser.json());

// secure Express app using Helmet
const helmet = require('helmet');

//set headers using helmet
app.use(helmet());

//connect to mongoose
mongoose.connect('mongodb://localhost/reflectiondb', {useNewUrlParser : true});

//set database connection variable 
var db = mongoose.connection;

// check if connected to database
if(!db)
{
    console.log("Error: Unable to connected to database");
}
else
{
    console.log("Success: Connected to database");
}

// Step 3 - Setup server port 
let port = process.env.PORT || 8083;

// Step 4 - Respond with hello world message on root URL
app.get('/', (req, res) => {
    res.send("Hello from Reflection API - built using Node, Express and Mongo");
})

// Use routes in the app here*
app.use('/api', routes);

// Step 5 - Start server
app.listen(port, () =>{
    console.log("Reflection API running on port " +port);
})